import os
import re
from zipfile import ZipFile

with ZipFile('data.zip') as zip:
    zip.extractall()
    with open('data.txt') as file_to_read:
        result_file = open(f'result.txt', 'a')
        one_message = ''
        game_id = '"6210441"'
        line_counter = 0
        full_number_of_lines = 4845529

        filtered_lines = []
        for i, line in enumerate(file_to_read.readlines()):
            line_counter += 1
            print('{0:.2f}%'.format((line_counter / full_number_of_lines * 100)))
            line = line.replace('\n', '')
            line = re.sub(r'\s+', '', line.rstrip())
            one_message = one_message + line
            if one_message.startswith('CreateTime') & one_message.endswith("}}"):
                if game_id in one_message:
                    result_file.write(one_message + "\n")
                one_message = ''
        result_file.close()
        os.remove('data.txt')
